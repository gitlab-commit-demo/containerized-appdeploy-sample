# Enhance Your Kubernetes CI/CD Pipelines with GitLab & Open Source

This project contains sample code related to my talk at GitLab Commit San Francisco.

This project contains a containerized pipeline. It deploys a sample app using Helm.
